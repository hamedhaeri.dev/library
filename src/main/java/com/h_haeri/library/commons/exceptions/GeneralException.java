package com.h_haeri.library.commons.exceptions;



//@ResponseStatus(HttpStatus.NOT_FOUND)
public class GeneralException extends RuntimeException{
    public GeneralException() {
        super("GeneralException");
    }
    public GeneralException(String message) {
        super(message);
    }
}
