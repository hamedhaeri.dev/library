package com.h_haeri.library.commons.exceptions;



//@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException{
    public NotFoundException() {
        super("not found exception");
    }
    public NotFoundException(String message) {
        super(message);
    }
}
