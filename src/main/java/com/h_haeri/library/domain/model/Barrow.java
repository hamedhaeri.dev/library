package com.h_haeri.library.domain.model;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Entity
@Data
@Builder
public class Barrow extends BaseModel {

    @ManyToOne
    @JoinColumn
    private ShelfBook shelfBook;

    @ManyToOne
    @JoinColumn
    private MemberNaturalPerson member;

    private BorrowStatus status;

    private Date barrowDate;
    private Date returnDate;

    private Date memberReturnDate;

    //todo exdent find by robat


}
