package com.h_haeri.library.domain.model;

import jakarta.persistence.Entity;
import lombok.Data;

@Entity
@Data
public class LegalPerson extends Person {

    private String name;

}
