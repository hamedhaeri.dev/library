package com.h_haeri.library.domain.model;

import jakarta.persistence.Entity;
import lombok.Data;

@Entity
@Data
public class OrganizationRoles extends  BaseModel {
    private String name;

}
