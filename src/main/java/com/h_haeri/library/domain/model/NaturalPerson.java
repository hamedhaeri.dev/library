package com.h_haeri.library.domain.model;

import jakarta.persistence.Entity;
import lombok.Builder;
import lombok.Data;

@Entity
@Data
public class NaturalPerson extends Person {

    private String firstName;
    private String lastName;
    private String naturalCode;

}
