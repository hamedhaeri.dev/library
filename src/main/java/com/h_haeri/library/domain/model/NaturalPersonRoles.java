package com.h_haeri.library.domain.model;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;

import java.util.Date;

@Entity
@Data
public class NaturalPersonRoles extends BaseModel  {

    @ManyToOne
    @JoinColumn
    private NaturalPerson naturalPerson;

    @ManyToOne
    @JoinColumn
    private OrganizationRoles roles;

    @ManyToOne
    @JoinColumn
    private LegalPerson legalPerson;

    private Date effectiveDate;
    private Date disableDate;

}
