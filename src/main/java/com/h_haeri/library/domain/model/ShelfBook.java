package com.h_haeri.library.domain.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Builder;
import lombok.Data;

@Entity
@Data
@Builder
public class ShelfBook extends  BaseModel {

    @ManyToOne
    @JoinColumn
    private Book book;

    @Column(nullable = false,  unique = true)
    private String shelfCode;

    private Boolean lock;

    //todo exdent find by robat



}
