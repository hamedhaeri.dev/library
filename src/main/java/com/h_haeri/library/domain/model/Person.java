package com.h_haeri.library.domain.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.Builder;
import lombok.Data;

@Entity
@Data
public class Person extends BaseModel {

    @Column(nullable = false)
    private String fullName;
    @Column(nullable = false)
    private String uniqCode;

}
