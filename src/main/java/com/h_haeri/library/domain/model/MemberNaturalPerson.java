package com.h_haeri.library.domain.model;

import jakarta.persistence.Entity;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Entity
@Data
@Builder
public class MemberNaturalPerson extends NaturalPerson {

    private String memberCode;

    private Date effectiveDate;
    private Date disableDate;

}
