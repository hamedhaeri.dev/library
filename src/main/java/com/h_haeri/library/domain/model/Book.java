package com.h_haeri.library.domain.model;

import jakarta.persistence.*;
import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Data
@Builder
public class Book extends BaseModel {

    @Column(nullable = false)
    private String faName;
    @Column(nullable = false)
    private String enName;

    @Column(nullable = false)
    @OneToMany
    private Set<NaturalPerson> writer;

    @ManyToOne
    private LegalPerson publisher;

    @Column(nullable = false)
    private Date releaseDate;

    @Column(nullable = false,  unique = true)
    private String isbn;


}
