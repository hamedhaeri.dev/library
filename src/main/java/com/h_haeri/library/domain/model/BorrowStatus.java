package com.h_haeri.library.domain.model;

public enum BorrowStatus {
    BORROW,
    RETURN
}
