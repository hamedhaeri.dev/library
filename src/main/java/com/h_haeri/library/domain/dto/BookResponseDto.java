package com.h_haeri.library.domain.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Data
public class BookResponseDto implements Serializable {
    private final Long id;
    private final String faName;
    private final String enName;
    private final Set<String> writerFullNames;
    private final Long publisherId;
    private final String publisherFullName;
    private final Date releaseDate;
    private final String isbn;
}
