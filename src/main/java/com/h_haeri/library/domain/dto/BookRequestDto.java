package com.h_haeri.library.domain.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Data
public class BookRequestDto implements Serializable {
    private final String faName;
    private final String enName;
    private final Set<Long> writerIds;
    private final Long publisherId;
    private final Date releaseDate;
    private final String isbn;


}
