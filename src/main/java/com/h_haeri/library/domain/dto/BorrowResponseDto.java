package com.h_haeri.library.domain.dto;

import java.util.Date;

public class BorrowResponseDto {
    private String trackCode;
    private Date returnDate;
}
