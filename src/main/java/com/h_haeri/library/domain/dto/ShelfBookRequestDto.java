package com.h_haeri.library.domain.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Data
public class ShelfBookRequestDto implements Serializable {

    private Long bookId;
    private Integer number;


}
