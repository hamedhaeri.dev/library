package com.h_haeri.library.domain.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class MemberRequestDto implements Serializable {
    private final String fullName;
    private final String firstName;
    private final String lastName;
    private final String naturalCode;
    private final String memberCode;
    private final Date effectiveDate;
    private final Date disableDate;
}
