package com.h_haeri.library.domain.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Data
public class ShelfBookResponseDto implements Serializable {
    private final Long id;
    private final Long bookId;
    private final String bookFaName;
    private final String bookEnName;
    private final Set<String> bookWriterFullNames;
    private final String bookPublisherFullName;
    private final Date bookReleaseDate;
    private final String bookIsbn;
    private final String shelfCode;
    private final Boolean lock;
}
