package com.h_haeri.library.domain.repo;

import com.h_haeri.library.domain.model.ShelfBook;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ShelfBookJpaRepository extends JpaRepository<ShelfBook, Long> {

    Optional<ShelfBook> findByShelfCode(String shelfCode);
}
