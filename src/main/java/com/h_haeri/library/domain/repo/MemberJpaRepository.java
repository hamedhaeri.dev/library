package com.h_haeri.library.domain.repo;

import com.h_haeri.library.domain.model.MemberNaturalPerson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface MemberJpaRepository extends JpaRepository<MemberNaturalPerson, Long> {

    List<MemberNaturalPerson> findAllByFullNameContainsOrNaturalCodeContainsOrMemberCodeContains
            (String fullName, String naturalCode, String memberCode);

    Optional<MemberNaturalPerson> findByMemberCode(String memberCode);
}
