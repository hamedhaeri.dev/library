package com.h_haeri.library.domain.repo;

import com.h_haeri.library.domain.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface BookJpaRepository extends JpaRepository<Book, Long> {

    Optional<Book> findByIsbn(String isbn);

    List<Book> findAllByEnNameOrFaNameOrIsbnOrWriterFullName(String enName, String faName, String isbn, String writer_fullName);

}
