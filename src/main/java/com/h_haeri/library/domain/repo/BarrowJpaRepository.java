package com.h_haeri.library.domain.repo;

import com.h_haeri.library.domain.model.Barrow;
import com.h_haeri.library.domain.model.BorrowStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface BarrowJpaRepository extends JpaRepository<Barrow, Long> {

    Optional<Barrow> findByStatusAndShelfBookShelfCodeAndMemberMemberCode(BorrowStatus status, String ShelfCode, String memberCode);
}
