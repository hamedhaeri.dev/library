package com.h_haeri.library.service.impl;

import com.h_haeri.library.commons.exceptions.GeneralException;
import com.h_haeri.library.commons.exceptions.NotFoundException;
import com.h_haeri.library.domain.dto.BookRequestDto;
import com.h_haeri.library.domain.dto.BookResponseDto;
import com.h_haeri.library.domain.dto.ShelfBookRequestDto;
import com.h_haeri.library.domain.dto.ShelfBookResponseDto;
import com.h_haeri.library.domain.model.Book;
import com.h_haeri.library.domain.model.ShelfBook;
import com.h_haeri.library.domain.repo.BookJpaRepository;
import com.h_haeri.library.domain.repo.ShelfBookJpaRepository;
import com.h_haeri.library.service.BookManagerUseCase;
import com.h_haeri.library.service.ShelfManagerUseCase;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
@AllArgsConstructor
public class ShelfManagerService implements ShelfManagerUseCase {

    private final ShelfBookJpaRepository repository;
    private final BookJpaRepository bookRepository;
    private ModelMapper modelMapper;

    @Override
    public List<ShelfBookResponseDto> fetchBook(String key) {
        return null;
    }

    @Override
    public List<ShelfBookResponseDto> addBookInShelf(ShelfBookRequestDto shelfbookDto) {
        Book book = bookRepository.findById(shelfbookDto.getBookId()).orElseThrow(() -> new GeneralException("Book IS EXIST"));

        List<ShelfBook> shelfBookList = new ArrayList<>();
        for (int i = 1; i <= shelfbookDto.getNumber().intValue(); i++
        ) {


            shelfBookList.add(repository.save(ShelfBook.builder()
                    .book(book)
                    .shelfCode(book.getIsbn() + String.valueOf(i))
                    .lock(false)
                    .build()));

        }

        return shelfBookList.stream().map(shelfBook -> modelMapper.map(shelfBook,ShelfBookResponseDto.class) ).collect(Collectors.toList());
    }

    @Override
    public Boolean removeBook(Long bookId) {
        return null;
    }
}
