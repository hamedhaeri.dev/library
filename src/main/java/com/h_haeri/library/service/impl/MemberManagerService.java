package com.h_haeri.library.service.impl;

import com.h_haeri.library.commons.exceptions.NotFoundException;
import com.h_haeri.library.domain.dto.MemberRequestDto;
import com.h_haeri.library.domain.dto.MemberResponseDto;
import com.h_haeri.library.domain.model.MemberNaturalPerson;
import com.h_haeri.library.domain.repo.MemberJpaRepository;
import com.h_haeri.library.service.MemberManagerUseCase;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class MemberManagerService implements MemberManagerUseCase {

    private final MemberJpaRepository repository;
    private ModelMapper modelMapper;

    @Override
    public List<MemberResponseDto> fetchMember(String key) {
        return repository.findAllByFullNameContainsOrNaturalCodeContainsOrMemberCodeContains(key, key, key)

                .stream().map(member -> modelMapper.map(
                        member,
                        MemberResponseDto.class
                )).collect(Collectors.toList());
    }

    @Override
    public MemberResponseDto addMember(MemberRequestDto memberDto) {

        MemberNaturalPerson memberNaturalPerson = MemberNaturalPerson
                .builder()
                .memberCode(memberDto.getMemberCode())
                .effectiveDate(memberDto.getEffectiveDate())
                .disableDate(memberDto.getDisableDate())
                .build();

        memberNaturalPerson.setUniqCode(memberDto.getNaturalCode());
        memberNaturalPerson.setFullName(memberDto.getFullName());
        memberNaturalPerson.setFirstName(memberDto.getFirstName());
        memberNaturalPerson.setLastName(memberDto.getLastName());


        return modelMapper.map(repository.save(memberNaturalPerson), MemberResponseDto.class);
    }

    @Override
    public MemberResponseDto editMember(Long memberCode, MemberRequestDto memberDto) {
        return null;
    }

    @Override
    public Boolean removeMember(String memberCode) {
        MemberNaturalPerson person = repository.findByMemberCode(memberCode).orElseThrow(NotFoundException::new);
        person.setDisableDate(new Date());

        repository.save(person);
        return true;
    }
}
