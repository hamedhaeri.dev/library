package com.h_haeri.library.service.impl;

import com.h_haeri.library.commons.exceptions.GeneralException;
import com.h_haeri.library.domain.dto.BorrowResponseDto;
import com.h_haeri.library.domain.model.Barrow;
import com.h_haeri.library.domain.model.BorrowStatus;
import com.h_haeri.library.domain.model.MemberNaturalPerson;
import com.h_haeri.library.domain.model.ShelfBook;
import com.h_haeri.library.domain.repo.BarrowJpaRepository;
import com.h_haeri.library.domain.repo.MemberJpaRepository;
import com.h_haeri.library.domain.repo.ShelfBookJpaRepository;
import com.h_haeri.library.service.BorrowManagerUserCase;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.time.DateUtils;

import java.util.Date;

@Service
@AllArgsConstructor
public class BorrowManagerService implements BorrowManagerUserCase {

    private final ShelfBookJpaRepository shelfRepository;
    private final MemberJpaRepository memberRepository;

    private final BarrowJpaRepository repository;
    private ModelMapper modelMapper;

    @Override
    public BorrowResponseDto borrowBook(String shelfCode, String memberCode) {
        ShelfBook shelfBook = shelfRepository.findByShelfCode(shelfCode).orElseThrow(() -> new GeneralException("Book IS EXIST"));
        MemberNaturalPerson person = memberRepository.findByMemberCode(memberCode).orElseThrow(() -> new GeneralException("Member IS EXIST"));

        Barrow barrow = repository.save(Barrow.builder()
                .shelfBook(shelfBook)
                .member(person)
                .barrowDate(new Date())
                .returnDate(DateUtils.addDays(new Date(), 2))
                .status(BorrowStatus.BORROW)
                .build());


        shelfBook.setLock(true);
        shelfRepository.save(shelfBook);


        return modelMapper.map(barrow, BorrowResponseDto.class);
    }

    @Override
    public Boolean returnBook(String bookCode, String memberCode) {

        Barrow barrow = repository.findByStatusAndShelfBookShelfCodeAndMemberMemberCode(BorrowStatus.BORROW, bookCode, memberCode).orElseThrow(() -> new GeneralException("Borrow IS EXIST"));

        barrow.setMemberReturnDate(new Date());
        barrow.setStatus(BorrowStatus.RETURN);

        repository.save(barrow);

        return true;
    }
}
