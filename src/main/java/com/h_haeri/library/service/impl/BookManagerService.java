package com.h_haeri.library.service.impl;

import com.h_haeri.library.commons.exceptions.GeneralException;
import com.h_haeri.library.commons.exceptions.NotFoundException;
import com.h_haeri.library.domain.dto.BookRequestDto;
import com.h_haeri.library.domain.dto.BookResponseDto;
import com.h_haeri.library.domain.model.Book;
import com.h_haeri.library.domain.repo.BookJpaRepository;
import com.h_haeri.library.service.BookManagerUseCase;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
@AllArgsConstructor
public class BookManagerService implements BookManagerUseCase {

    private final BookJpaRepository repository;
    private ModelMapper modelMapper;

    @Override
    public List<BookResponseDto> fetchBook(String key) {

        return repository.findAllByEnNameOrFaNameOrIsbnOrWriterFullName(key, key, key, key).stream()
                .map(book -> modelMapper.map(book, BookResponseDto.class)).collect(Collectors.toList());
    }

    @Override
    public BookResponseDto addBook(BookRequestDto book) {

        repository.findByIsbn(book.getIsbn()).orElseThrow(() -> new GeneralException("Book IS EXIST"));

        return modelMapper.map(repository.save(Book.builder()
                .faName(book.getFaName())
                .enName(book.getEnName())
                .isbn(book.getIsbn())

                .build()), BookResponseDto.class);
    }

    @Override
    public BookResponseDto editBook(Long bookId, BookRequestDto bookDto) {
        Book book = repository.findById(bookId).orElseThrow(NotFoundException::new);

        book.setFaName(bookDto.getFaName());

        return modelMapper.map(repository.save(book), BookResponseDto.class);
    }

    @Override
    public Boolean removeBook(Long bookId) {
        Book book = repository.findById(bookId).orElseThrow(NotFoundException::new);
        repository.delete(book);
        return true;
    }
}
