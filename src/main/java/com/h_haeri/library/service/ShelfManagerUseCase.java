package com.h_haeri.library.service;

import com.h_haeri.library.domain.dto.ShelfBookRequestDto;
import com.h_haeri.library.domain.dto.ShelfBookResponseDto;

import java.util.List;


public interface ShelfManagerUseCase {
     List<ShelfBookResponseDto> fetchBook(String key);
     List<ShelfBookResponseDto> addBookInShelf(ShelfBookRequestDto shelfbookDto);
     Boolean removeBook(Long bookId);
}
