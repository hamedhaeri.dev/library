package com.h_haeri.library.service;

import com.h_haeri.library.domain.dto.BookRequestDto;
import com.h_haeri.library.domain.dto.BookResponseDto;
import com.h_haeri.library.domain.model.Book;

import java.util.List;


public interface BookManagerUseCase {
     List<BookResponseDto> fetchBook(String key);
     BookResponseDto addBook(BookRequestDto book);
     BookResponseDto editBook(Long bookId,BookRequestDto book);
     Boolean removeBook(Long bookId);
}
