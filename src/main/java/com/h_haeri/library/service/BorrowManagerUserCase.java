package com.h_haeri.library.service;

import com.h_haeri.library.domain.dto.BorrowResponseDto;

public interface BorrowManagerUserCase {
    BorrowResponseDto borrowBook(String shelfCode, String memberCode);
    Boolean returnBook(String bookCode,String memberCode);
}
