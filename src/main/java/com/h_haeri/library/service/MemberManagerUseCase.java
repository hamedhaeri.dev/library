package com.h_haeri.library.service;

import com.h_haeri.library.domain.dto.BookRequestDto;
import com.h_haeri.library.domain.dto.BookResponseDto;
import com.h_haeri.library.domain.dto.MemberRequestDto;
import com.h_haeri.library.domain.dto.MemberResponseDto;

import java.util.List;


public interface MemberManagerUseCase {
     List<MemberResponseDto> fetchMember(String key);
     MemberResponseDto addMember(MemberRequestDto memberDto);
     MemberResponseDto editMember(Long memberCode,MemberRequestDto memberDto);
     Boolean removeMember(String memberCode);
}
